package com.github.zxhtom.crontab.mapper;

import com.github.zxhtom.crontab.model.Cron;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @package com.github.zxhtom.crontab.mapper
 * @Class CronMapper
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-11-15 上午10:20
 */
@Mapper
public interface CronMapper {

    @Select("select * from cron")
    public List<Cron> getCron();
}
