package com.github.zxhtom.crontab.config;


import com.github.zxhtom.crontab.mapper.CronMapper;
import com.github.zxhtom.crontab.mapper.TestMapper;
import com.github.zxhtom.crontab.model.Cron;
import com.github.zxhtom.crontab.model.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @package com.github.zxhtom.crontab.config
 * @Class ScheduleConfigV1
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-11-15 上午10:19
 */
//@Configuration
public class ScheduleConfigV1 implements SchedulingConfigurer {

    @Autowired
    CronMapper cronMapper;
    @Autowired
    TestMapper testMapper;
    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {

        scheduledTaskRegistrar.addTriggerTask(()-> {
                    System.out.println("执行定时器任务:" + LocalDateTime.now().toLocalTime());
                    List<Test> tests = testMapper.getTests();
                    System.out.println(tests);
                },
                triggerContext -> {
                    List<Cron> crons = cronMapper.getCron();
                    Cron cron = crons.get(0);
                    return new CronTrigger(cron.getCron()).nextExecutionTime(triggerContext);
                });
    }
}
